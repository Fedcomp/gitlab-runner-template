# Gitlab runner template

My own template for gitlab runner for my own needs.
Starts privileged docker dind as a side service to the runner.

## Starting up

Just run:
```bash
touch config.toml
docker-compose run --rm runner register
```
and follow instructions to register runner in your instance.
Make sure to set privileged = true in resulting config.yml.

Then just start everything with:
```bash
docker-compose up -d
```

Make sure runner works without error:
```bash
docker-compose logs -f runner
```
